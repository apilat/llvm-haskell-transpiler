int fib(int x) {
    int a = 1, b = 1, c = 1, d = 0;
    int s = 1, t = 1, u = 1, v = 0;
    while (x > 0) {
        if (x & 1) {
            int a2 = s * a + t * c, b2 = s * b + t * d, c2 = s * a + t * c, d2 = u * b + v * d;
            a = a2, b = b2, c = c2, d = d2;
        }
        int s2 = s * s + t * u, t2 = s * t + t * v, u2 = u * s + v * u, v2 = u * t + v * v;
        s = s2, t = t2, u = u2, v = v2;
        x /= 2;
    }
    return d;
}
