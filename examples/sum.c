int square_sum(int n) {
    int x = 0;
    for (int i = 0; i <= n; i++) {
        x += i * i;
    }
    return x;
}
