use llvm_ir::{
    instruction::{groups::BinaryOp, BinaryOp as _, HasResult},
    types::{Typed, Types},
    Constant, Function, Instruction, IntPredicate, Module, Name, Operand, Terminator, Type,
    TypeRef,
};
use std::fmt::Write;

fn main() {
    let module = Module::from_bc_path("examples/fib_matrix.bc").expect("failed to parse bytecode");
    generate_standard_imports();
    for function in module.functions {
        generate_function(&module.types, function);
    }
}

fn generate_standard_imports() {
    println!("import Data.Bits");
}

#[derive(Debug, Clone)]
struct State<'p> {
    prefix_fn: String,
    prefix_type: String,
    variables: Vec<(&'p Name, TypeRef)>,
    blocks: Vec<&'p Name>,
    inst_index: usize,
    block_index: usize,
    state_expr: String,
}

impl<'p> State<'p> {
    fn variable_list(&self) -> String {
        format!(
            "({} {})",
            self.prefix_type,
            gen_list(self.variables.len(), " ", |s, i| write!(s, "_{i}").unwrap())
        )
    }

    fn variable_list_with(&self, idx: usize, val: &str) -> String {
        format!(
            "({} {})",
            self.prefix_type,
            gen_list(self.variables.len(), " ", |s, i| {
                if i == idx {
                    s.push_str(val)
                } else {
                    write!(s, "_{i}").unwrap()
                }
            })
        )
    }

    fn add_inst_modify(&mut self, idx: usize, value: &str) {
        println!(
            "{}_{} {} = {}",
            self.prefix_fn,
            self.inst_index,
            self.variable_list(),
            self.variable_list_with(idx, value)
        );
        self.state_expr = format!(
            "({}_{} {})",
            self.prefix_fn, self.inst_index, self.state_expr
        );
        self.inst_index += 1;
    }

    fn add_block(&mut self, return_value: &str) {
        println!(
            "{}_b{} _a = let {} = {} in {}",
            self.prefix_fn,
            self.block_index,
            self.variable_list(),
            std::mem::replace(&mut self.state_expr, String::from("_a")),
            return_value
        );
        self.block_index += 1;
    }

    fn new(name: &'p str) -> Self {
        let mut iter = name.chars();
        let first = iter.next().expect("empty function name");
        let end = iter.collect::<String>();
        Self {
            prefix_fn: first.to_lowercase().collect::<String>() + &end,
            prefix_type: first.to_uppercase().collect::<String>() + &end,
            variables: Vec::new(),
            blocks: Vec::new(),
            inst_index: 0,
            block_index: 0,
            state_expr: String::from("_a"),
        }
    }
}

fn gen_list(len: usize, sep: &str, mut add: impl FnMut(&mut String, usize)) -> String {
    let mut s = String::new();
    for i in 0..len {
        if i > 0 {
            s.push_str(sep);
        }
        add(&mut s, i);
    }
    s
}

fn generate_function(types: &Types, function: Function) {
    let mut state = State::new(&function.name);

    for param in function.parameters.iter() {
        state.variables.push((&param.name, param.ty.clone()));
    }
    for bb in function.basic_blocks.iter() {
        for instr in bb.instrs.iter() {
            if let Some(name) = instr.try_get_result() {
                state.variables.push((name, instr.get_type(types)));
            }
        }
        state.blocks.push(&bb.name);
    }

    {
        let mut s = String::new();
        write!(s, "data {name} = {name}", name = state.prefix_type).unwrap();
        for (_, ty) in state.variables.iter() {
            write!(s, " {}", get_type_symbol(ty)).unwrap();
        }
        println!("{s}");
    }

    for bb in function.basic_blocks.iter() {
        for instr in bb.instrs.iter() {
            match instr {
                Instruction::Store(instr) => {
                    let idx = get_var_idx_from_operand(&state, &instr.address);
                    let value = get_operand(&state, &instr.value);
                    state.add_inst_modify(idx, &value);
                }
                Instruction::Load(instr) => {
                    let idx = get_var_idx_from_name(&state, &instr.dest);
                    let value = get_operand(&state, &instr.address);
                    state.add_inst_modify(idx, &value);
                }
                Instruction::ICmp(instr) => {
                    let idx = get_var_idx_from_name(&state, &instr.dest);
                    let value = format!(
                        "({} {} {})",
                        get_cmp_symbol(instr.predicate),
                        get_operand(&state, &instr.operand0),
                        get_operand(&state, &instr.operand1)
                    );
                    state.add_inst_modify(idx, &value);
                }
                Instruction::Call(instr) => {
                    let idx = get_var_idx_from_name(
                        &state,
                        instr.dest.as_ref().expect("void return unsupported"),
                    );
                    let name = get_called_fn_name(
                        instr
                            .function
                            .as_ref()
                            .right()
                            .expect("inline assembly not supported"),
                    );
                    let value = {
                        let mut s = String::new();
                        s.push('(');
                        s.push_str(name);
                        for (op, _attr) in instr.arguments.iter() {
                            s.push(' ');
                            s.push_str(&get_operand(&state, op));
                        }
                        s.push(')');
                        s
                    };
                    state.add_inst_modify(idx, &value);
                }
                Instruction::Alloca(_) => (),
                instr if instr.is_binary_op() => {
                    let instr: BinaryOp = instr.clone().try_into().unwrap();
                    let idx = get_var_idx_from_name(&state, instr.get_result());
                    let value = format!(
                        "({} {} {})",
                        get_binaryop_symbol(&instr),
                        get_operand(&state, instr.get_operand0()),
                        get_operand(&state, instr.get_operand1())
                    );
                    state.add_inst_modify(idx, &value);
                }
                _ => todo!("unsupported instruction {instr:?}"),
            }
        }

        match &bb.term {
            Terminator::Ret(term) => {
                let value = get_operand(
                    &state,
                    term.return_operand
                        .as_ref()
                        .expect("ret: void return unsupported"),
                );
                state.add_block(&value);
            }
            Terminator::Br(term) => {
                let target_idx = get_blk_idx_from_name(&state, &term.dest);
                state.add_block(&format!(
                    "{}_b{} {}",
                    state.prefix_fn,
                    target_idx,
                    state.variable_list()
                ));
            }
            Terminator::CondBr(term) => {
                let cond = get_operand(&state, &term.condition);
                let true_idx = get_blk_idx_from_name(&state, &term.true_dest);
                let false_idx = get_blk_idx_from_name(&state, &term.false_dest);
                state.add_block(&format!(
                    "if {cond} then {pre}_b{true_idx} {list} else {pre}_b{false_idx} {list}",
                    pre = state.prefix_fn,
                    list = state.variable_list()
                ));
            }
            term => todo!("unsupported terminator {term:?}"),
        }
    }

    assert!(
        !function.basic_blocks.is_empty(),
        "function has no basic blocks"
    );
    println!(
        "{} {} = {}_b0 ({} {})",
        state.prefix_fn,
        gen_list(function.parameters.len(), " ", |s, i| write!(s, "_{i}")
            .unwrap()),
        state.prefix_fn,
        state.prefix_type,
        gen_list(state.variables.len(), " ", |s, i| {
            if i < function.parameters.len() {
                write!(s, "_{i}").unwrap()
            } else {
                s.push_str(get_type_default_value(&state.variables[i].1))
            }
        })
    );
}

fn get_type_symbol(ty: &Type) -> &'static str {
    match ty {
        Type::IntegerType { bits: 32 | 64 } => "Int",
        Type::IntegerType { bits: 1 } => "Bool",
        Type::PointerType {
            pointee_type,
            addr_space: _,
        } => get_type_symbol(pointee_type),
        _ => todo!("get_type_symbol: unsupported type {ty:?}"),
    }
}

fn get_type_default_value(ty: &Type) -> &'static str {
    match ty {
        Type::IntegerType { bits: 32 | 64 } => "0",
        Type::IntegerType { bits: 1 } => "False",
        Type::PointerType {
            pointee_type,
            addr_space: _,
        } => get_type_default_value(pointee_type),
        ty => todo!("get_type_default_value: unsupported type {ty:?}"),
    }
}

fn get_var_idx_from_name(state: &State, name: &Name) -> usize {
    if let Some(idx) = state.variables.iter().position(|(vname, _)| *vname == name) {
        idx
    } else {
        panic!("get_var_idx_from_name: name {name:?} not found");
    }
}

fn get_var_idx_from_operand(state: &State, operand: &Operand) -> usize {
    if let Operand::LocalOperand { name, ty: _ } = operand {
        get_var_idx_from_name(state, name)
    } else {
        panic!("get_var_idx_from_operand: {operand:?} is not local operand");
    }
}

fn get_blk_idx_from_name(state: &State, name: &Name) -> usize {
    if let Some(idx) = state.blocks.iter().position(|bname| *bname == name) {
        idx
    } else {
        panic!("get_blk_idx_from_name: name {name:?} not found")
    }
}

fn get_called_fn_name(op: &Operand) -> &str {
    let op = if let Operand::ConstantOperand(op) = op {
        op
    } else {
        panic!("get_called_fn_name: {op:?} is not constant")
    };
    let name = if let Constant::GlobalReference { name, ty: _ } = op.as_ref() {
        name
    } else {
        panic!("get_called_fn_name: {op:?} is not a global reference")
    };
    if let Name::Name(name) = name {
        name
    } else {
        panic!("get_called_fn_name: {name:?} is not a string")
    }
}

fn get_operand(state: &State, operand: &Operand) -> String {
    match operand {
        Operand::LocalOperand { name, ty: _ } => {
            // This is inefficient if there are lots of variables.
            if let Some(idx) = state.variables.iter().position(|(vname, _)| *vname == name) {
                format!("_{idx}")
            } else {
                panic!("get_operand: operand {name:?} not found")
            }
        }
        Operand::ConstantOperand(val) => match val.as_ref() {
            Constant::Int { bits: 32, value } => value.to_string(),
            val => todo!("get_operand: unsupported constant {val:?}"),
        },
        op => todo!("get_operand: unsupported operand {op:?}"),
    }
}

fn get_binaryop_symbol(op: &BinaryOp) -> &'static str {
    use BinaryOp::*;
    match op {
        Add(_) => "(+)",
        Sub(_) => "(-)",
        Mul(_) => "(*)",
        UDiv(_) | SDiv(_) => "div",
        And(_) => "(.&.)",
        Or(_) => "(.|.)",
        Xor(_) => "xor",
        _ => todo!("get_binaryop_symbol: unsupported operation {op:?}"),
    }
}

fn get_cmp_symbol(cmp: IntPredicate) -> &'static str {
    use IntPredicate::*;
    match cmp {
        EQ => "(==)",
        NE => "(/=)",
        UGT | SGT => "(>)",
        UGE | SGE => "(>=)",
        ULT | SLT => "(<)",
        ULE | SLE => "(<=)",
    }
}
